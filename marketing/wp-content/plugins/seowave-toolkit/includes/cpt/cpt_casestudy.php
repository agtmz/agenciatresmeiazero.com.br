<?php

/* CPT : Case Study */
if ( ! function_exists('ow_cpt_casestudy') ) {

	function ow_cpt_casestudy() {

		$labels = array(
			'name' =>  __('Case Study', 'seowave' ),
			'singular_name' => __('Case Study', 'seowave' ),
			'add_new' => __('Add New', 'seowave' ),
			'add_new_item' => __('Add New Case Study', 'seowave' ),
			'edit_item' => __('Edit Case Study', 'seowave' ),
			'new_item' => __('New Case Study', 'seowave' ),
			'all_items' => __('All Case Study', 'seowave' ),
			'view_item' => __('View Case Study', 'seowave' ),
			'search_items' => __('Search Case Study', 'seowave' ),
			'not_found' =>  __('No Case Study found', 'seowave' ),
			'not_found_in_trash' => __('No Case Study found in Trash', 'seowave' ),
			'parent_item_colon' => '',
			'menu_name' => __('Case Study', 'seowave')
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'query_var' => true,
			'rewrite'  => array( 'slug' => 'casestudy-item' ),
			'has_archive' => true, 
			'capability_type' => 'post', 
			'hierarchical' => true,
			'menu_position' => 106,
			'menu_icon' => 'dashicons-portfolio',
			'supports' => array( 'title', 'thumbnail','editor' )
		);
		register_post_type( 'ow_casestudy', $args );
	}
	add_action( 'init', 'ow_cpt_casestudy', 0 );
}

// Register Custom Taxonomy
function ow_tax_casestudy() {

	$labels = array(
		'name'                       => _x( 'Case Study Categories', 'Taxonomy General Name', 'text-domain' ),
		'singular_name'              => _x( 'Case Study Categories', 'Taxonomy Singular Name', 'text-domain' ),
		'menu_name'                  => __( 'Case Study Category', 'text-domain' ),
		'all_items'                  => __( 'All Items', 'text-domain' ),
		'parent_item'                => __( 'Parent Item', 'text-domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text-domain' ),
		'new_item_name'              => __( 'New Item Name', 'text-domain' ),
		'add_new_item'               => __( 'Add New Item', 'text-domain' ),
		'edit_item'                  => __( 'Edit Item', 'text-domain' ),
		'update_item'                => __( 'Update Item', 'text-domain' ),
		'view_item'                  => __( 'View Item', 'text-domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text-domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text-domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text-domain' ),
		'popular_items'              => __( 'Popular Items', 'text-domain' ),
		'search_items'               => __( 'Search Items', 'text-domain' ),
		'not_found'                  => __( 'Not Found', 'text-domain' ),
		'items_list'                 => __( 'Items list', 'text-domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text-domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'ow_casestudy_tax', array( 'ow_casestudy' ), $args );

}
add_action( 'init', 'ow_tax_casestudy', 0 );