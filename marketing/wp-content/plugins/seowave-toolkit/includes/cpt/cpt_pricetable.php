<?php

/* CPT : Price Table */
if ( ! function_exists('ow_cpt_pricetable') ) {

	function ow_cpt_pricetable() {

		$labels = array(
			'name' =>  __('Price Table', 'seowave' ),
			'singular_name' => __('Price Table', 'seowave' ),
			'add_new' => __('Add New', 'seowave' ),
			'add_new_item' => __('Add New Price Table', 'seowave' ),
			'edit_item' => __('Edit Price Table', 'seowave' ),
			'new_item' => __('New Price Table', 'seowave' ),
			'all_items' => __('All Price Table', 'seowave' ),
			'view_item' => __('View Price Table', 'seowave' ),
			'search_items' => __('Search Price Table', 'seowave' ),
			'not_found' =>  __('No Case Price Table', 'seowave' ),
			'not_found_in_trash' => __('No Price Table found in Trash', 'seowave' ),
			'parent_item_colon' => '',
			'menu_name' => __('Price Table', 'seowave' )
		);

		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'show_in_menu' => true, 
			'query_var' => true,
			'rewrite'  => array( 'slug' => 'pricetable-item' ),
			'has_archive' => true, 
			'capability_type' => 'post', 
			'hierarchical' => true,
			'menu_position' => 106,
			'menu_icon' => 'dashicons-portfolio',
			'supports' => array( 'title')
		);
		register_post_type( 'ow_pricetable', $args );
	}
	add_action( 'init', 'ow_cpt_pricetable', 0 );
}

// Register Custom Taxonomy
function ow_tax_pricetable() {

	$labels = array(
		'name'                       => _x( 'Price Table Categories', 'Taxonomy General Name', 'text-domain' ),
		'singular_name'              => _x( 'Price Table Categories', 'Taxonomy Singular Name', 'text-domain' ),
		'menu_name'                  => __( 'Price Table Category', 'text-domain' ),
		'all_items'                  => __( 'All Items', 'text-domain' ),
		'parent_item'                => __( 'Parent Item', 'text-domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text-domain' ),
		'new_item_name'              => __( 'New Item Name', 'text-domain' ),
		'add_new_item'               => __( 'Add New Item', 'text-domain' ),
		'edit_item'                  => __( 'Edit Item', 'text-domain' ),
		'update_item'                => __( 'Update Item', 'text-domain' ),
		'view_item'                  => __( 'View Item', 'text-domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text-domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text-domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text-domain' ),
		'popular_items'              => __( 'Popular Items', 'text-domain' ),
		'search_items'               => __( 'Search Items', 'text-domain' ),
		'not_found'                  => __( 'Not Found', 'text-domain' ),
		'items_list'                 => __( 'Items list', 'text-domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text-domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'ow_pricetable_taxonomy', array( 'ow_pricetable' ), $args );

}
add_action( 'init', 'ow_tax_pricetable', 0 );