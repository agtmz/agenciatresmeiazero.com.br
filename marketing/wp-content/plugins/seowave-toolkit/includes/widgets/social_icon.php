<?php
/**
 * Social Icons Widget class
 *
 * @since 1.0
 */
class OW_Widget_Social_Icons extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_social_icons', 'description' => __( 'A Social Icons Widget.', 'seowave' ) );
		parent::__construct( 'social_icons', _x( 'OW :: Social Icons', 'Social Icons widget' , 'seowave' ), $widget_ops );
	}

	public function widget( $args, $instance ) {

		echo html_entity_decode( $args['before_widget'] ); // Widget starts to print information

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

		if ( $title ) {
			echo html_entity_decode( $args['before_title'] . $title . $args['after_title'] );
		}

		$social_facebook = empty( $instance['social_facebook'] ) ? '' : $instance['social_facebook'];
		$social_twitter = empty( $instance['social_twitter'] ) ? '' : $instance['social_twitter'];
		$social_googleplus = empty( $instance['social_googleplus'] ) ? '' : $instance['social_googleplus'];
		$social_instagram = empty( $instance['social_instagram'] ) ? '' : $instance['social_instagram'];
		$social_linkedin = empty( $instance['social_linkedin'] ) ? '' : $instance['social_linkedin'];
		$social_rss = empty( $instance['social_rss'] ) ? '' : $instance['social_rss'];
		?>
		<ul class="socials">
			<?php if( !empty( $social_facebook ) ): ?><li><a target="_blank" href="<?php echo esc_url( $social_facebook ); ?>"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
			<?php if( !empty( $social_twitter ) ): ?><li><a target="_blank" href="<?php echo esc_url( $social_twitter ); ?>"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
			<?php if( !empty( $social_googleplus ) ): ?><li><a target="_blank" href="<?php echo esc_url( $social_googleplus ); ?>"><i class="fa fa-google-plus"></i></a></li><?php endif; ?>
			<?php if( !empty( $social_instagram ) ): ?><li><a target="_blank" href="<?php echo esc_url( $social_instagram ); ?>"><i class="fa fa-instagram"></i></a></li><?php endif; ?>
			<?php if( !empty( $social_linkedin ) ): ?><li><a target="_blank" href="<?php echo esc_url( $social_linkedin ); ?>"><i class="fa fa-linkedin"></i></a></li><?php endif; ?>
			<?php if( !empty( $social_rss ) ): ?><li><a target="_blank" href="<?php echo esc_url( $social_rss ); ?>"><i class="fa fa-rss"></i></a></li><?php endif; ?>
		</ul>
		<?php
		echo html_entity_decode( $args['after_widget'] ); // Widget ends printing information
	}

	public function form( $instance ) {

		$instance = wp_parse_args( ( array ) $instance, array( 'title' => '' ) );

		$title = $instance['title'];

		$social_facebook = empty( $instance['social_facebook'] ) ? '' : $instance['social_facebook'];
		$social_twitter = empty( $instance['social_twitter'] ) ? '' : $instance['social_twitter'];
		$social_googleplus = empty( $instance['social_googleplus'] ) ? '' : $instance['social_googleplus'];
		$social_instagram = empty( $instance['social_instagram'] ) ? '' : $instance['social_instagram'];
		$social_linkedin = empty( $instance['social_linkedin'] ) ? '' : $instance['social_linkedin'];
		$social_rss = empty( $instance['social_rss'] ) ? '' : $instance['social_rss'];
		
		?>
		<p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php _e('Title:', 'seowave' ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('title') ); ?>" name="<?php echo esc_html( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_facebook') ); ?>"><?php _e('Facebook:', 'seowave' ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_facebook') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_facebook') ); ?>" type="text" value="<?php echo esc_url( $social_facebook ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_twitter') ); ?>"><?php _e('Twitter:', 'seowave' ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_twitter') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_twitter') ); ?>" type="text" value="<?php echo esc_url( $social_twitter ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_googleplus') ); ?>"><?php _e('Google Plus:', 'seowave' ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_googleplus') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_googleplus') ); ?>" type="text" value="<?php echo esc_url( $social_googleplus ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_instagram') ); ?>"><?php _e('instagram', 'seowave' ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_instagram') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_instagram') ); ?>" type="text" value="<?php echo esc_url( $social_instagram ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_linkedin') ); ?>"><?php _e('Linkedin', 'seowave' ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_linkedin') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_linkedin') ); ?>" type="text" value="<?php echo esc_url( $social_linkedin ); ?>" /></label></p>
		<p><label for="<?php echo esc_attr( $this->get_field_id('social_rss') ); ?>"><?php _e('Rss:', 'seowave' ); ?> <input class="widefat" id="<?php echo esc_html( $this->get_field_id('social_rss') ); ?>" name="<?php echo esc_html( $this->get_field_name('social_rss') ); ?>" type="text" value="<?php echo esc_url( $social_rss ); ?>" /></label></p>
		
		
		<?php
	}

	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		$new_instance = wp_parse_args((array) $new_instance, array( 'title' => ''));

		$instance['title'] = strip_tags($new_instance['title']);

		$instance['social_facebook'] = ( ! empty( $new_instance['social_facebook'] ) ) ? strip_tags( $new_instance['social_facebook'] ) : '';
		$instance['social_twitter'] = ( ! empty( $new_instance['social_twitter'] ) ) ? strip_tags( $new_instance['social_twitter'] ) : ''; 
		$instance['social_googleplus'] = ( ! empty( $new_instance['social_googleplus'] ) ) ? strip_tags( $new_instance['social_googleplus'] ) : ''; 
		$instance['social_instagram'] = ( ! empty( $new_instance['social_instagram'] ) ) ? strip_tags( $new_instance['social_instagram'] ) : ''; 
		$instance['social_linkedin'] = ( ! empty( $new_instance['social_linkedin'] ) ) ? strip_tags( $new_instance['social_linkedin'] ) : ''; 
		$instance['social_rss'] = ( ! empty( $new_instance['social_rss'] ) ) ? strip_tags( $new_instance['social_rss'] ) : ''; 

		return $instance;
	}
}
?>
