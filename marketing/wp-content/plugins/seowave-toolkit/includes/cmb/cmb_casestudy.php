<?php
// Start with an underscore to hide fields from custom fields list
$prefix = 'ow_cf_';

$cmb_casestudy = new_cmb2_box( array(
	'id'            => $prefix . 'metabox_casestudy',
	'title'         => __( 'Option', 'seowave' ),
	'object_types'  => array( 'ow_casestudy', ), // Post type
	'context'       => 'normal',
	'priority'      => 'high',
	'show_names'    => true, // Show field names on the left
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Description', 'seowave' ),
	'id'           => $prefix . 'casestudy_desc',
	'type'    => 'wysiwyg',
    'options' => array(
        'wpautop' => true, // use wpautop?
        'media_buttons' => true, // show insert/upload button(s)
        'textarea_rows' => get_option('default_post_edit_rows', 4), // rows="..."
        'tabindex' => '',
        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
        'editor_class' => '', // add extra class(es) to the editor textarea
        'teeny' => false, // output the minimal editor config used in Press This
        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
    ),
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Portfolio Big Image ', 'seowave' ),
	'id'           => $prefix . 'portfolio_bigimage',
	'type'         => 'file',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Client Name ', 'seowave' ),
	'id'           => $prefix . 'client_name',
	'type'         => 'text',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Project Type ', 'seowave' ),
	'id'           => $prefix . 'project_type',
	'type'         => 'text',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Website URL ', 'seowave' ),
	'id'           => $prefix . 'web_url',
	'type'         => 'text',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Counter Value Left Part', 'seowave' ),
	'id'           => $prefix . 'counter_value',
	'type'         => 'text',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Counter Title Left Part', 'seowave' ),
	'id'           => $prefix . 'counter_title',
	'type'         => 'text',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Counter Left Graph Image ', 'seowave' ),
	'id'           => $prefix . 'counter_img',
	'type'         => 'file',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Counter Value Right ', 'seowave' ),
	'id'           => $prefix . 'counter_valueright',
	'type'         => 'text',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Counter Title Right Part', 'seowave' ),
	'id'           => $prefix . 'counter_titleright',
	'type'         => 'text',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Counter Right Graph Image ', 'seowave' ),
	'id'           => $prefix . 'counter_imgright',
	'type'         => 'file',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Challenge Description', 'seowave' ),
	'id'           => $prefix . 'challenge_desc',
	'type'    => 'wysiwyg',
    'options' => array(
        'wpautop' => true, // use wpautop?
        'media_buttons' => true, // show insert/upload button(s)
        'textarea_rows' => get_option('default_post_edit_rows', 4), // rows="..."
        'tabindex' => '',
        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
        'editor_class' => '', // add extra class(es) to the editor textarea
        'teeny' => false, // output the minimal editor config used in Press This
        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
    ),
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Solution Description', 'seowave' ),
	'id'           => $prefix . 'solution_desc',
	'type'    => 'wysiwyg',
    'options' => array(
        'wpautop' => true, // use wpautop?
        'media_buttons' => true, // show insert/upload button(s)
        'textarea_rows' => get_option('default_post_edit_rows', 4), // rows="..."
        'tabindex' => '',
        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
        'editor_class' => '', // add extra class(es) to the editor textarea
        'teeny' => false, // output the minimal editor config used in Press This
        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
    ),
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Single Details Image ', 'seowave' ),
	'id'           => $prefix . 'single_img',
	'type'         => 'file',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Result Graph Image ', 'seowave' ),
	'id'           => $prefix . 'result_img',
	'type'         => 'file',		
) );

$cmb_casestudy->add_field( array(
	'name'         => __( 'Result Description', 'seowave' ),
	'id'           => $prefix . 'result_desc',
	'type'    => 'wysiwyg',
    'options' => array(
        'wpautop' => true, // use wpautop?
        'media_buttons' => true, // show insert/upload button(s)
        'textarea_rows' => get_option('default_post_edit_rows', 4), // rows="..."
        'tabindex' => '',
        'editor_css' => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the `<style>` tags, can use "scoped".
        'editor_class' => '', // add extra class(es) to the editor textarea
        'teeny' => false, // output the minimal editor config used in Press This
        'dfw' => false, // replace the default fullscreen with DFW (needs specific css)
        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
    ),
) );
?>