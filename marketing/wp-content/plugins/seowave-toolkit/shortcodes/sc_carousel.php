<?php
function sc_carousel_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'extra_class' => ''

	), $atts ) );

	$result = "
		<div class='owl-carousel offer_service_carousel $extra_class'>
		   ".do_shortcode( $content )."
		</div>";

	return $result;
}
add_shortcode( 'carousel_outer', 'sc_carousel_outer' );

function sc_carousel_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'iconimg' => '',
		'title' => '',
		'carouselimg' => '',
		'extra_class' => ''

	), $atts ) );
	
	$carousel_img = wp_get_attachment_image( $carouselimg, 'seowave-403-439' );

	$result = "
		<div class='item $extra_class'>
			$carousel_img
		</div>";

	return $result;
}
add_shortcode( 'carousel_inner', 'sc_carousel_inner' );

// Parent Element
function vc_carousel_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Image Carousel", "seowave"),
		"base" => "carousel_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'carousel_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			
			array(
				"type" => "label",
				"class" => "",
				"heading" => esc_html__("No need of settings", "seowave"),
				"param_name" => "label_imagecrousal",
			),
			
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_carousel_outer' );

// Nested Element
function vc_carousel_inner() {

	vc_map( array(
		"name" => __("Single Carousel", "seowave"),
		"base" => "carousel_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'carousel_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			
			array(
				"type" => "attach_image",
				"heading" => __("Slider Image", "seowave"),
				"param_name" => "carouselimg",
			),
		)
	) );
}
add_action( 'vc_before_init', 'vc_carousel_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Carousel_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Carousel_Inner extends WPBakeryShortCode {
    }
}
?>