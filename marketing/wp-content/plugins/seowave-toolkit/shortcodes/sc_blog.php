<?php
function ow_blog( $atts ) {

	extract( shortcode_atts(
		array(
			'title' => '',
			'desc' => '',
			'btntxt' => '',
			'btnurl' => '',
			'posts_display' => ''
		), $atts )
	);

	/* Post Arguments */
	$args = array(
		'posts_per_page' => $posts_display,
		'ignore_sticky_posts' => 1
	);
	
	$qry = new WP_Query( $args );
	
	if( '' === $posts_display ) :
	
		$posts_display = 3;
		
	endif;
	
	ob_start();

	?>
	
	<section class="row latest_blogs contentRow">
		<div class="container">
			<div class="row title_row">
				<?php
					echo seowave_content('<h2>','</h2>', esc_attr( $title ) );
					echo seowave_content('<h5>','</h5>', esc_attr( $desc ) );
				?>
			</div>
			
			<div class="row">
			<?php 
				while ( $qry->have_posts() ) : $qry->the_post();
					?>
					<div class="col-sm-4 blog latest_blog">
						<div class="m0">
							<div class="m0 image"> 
								<?php the_post_thumbnail( 'seowave-370-266' ); ?>
							</div>
							<div class="media">
								<div class="media-left post_cat">
									<?php
										if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_post_icon_id', true ) ) ) :
											?>
											<div class="post-icon">
											<?php
												echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_post_icon_id', true), 'seowave-26-26' );
												
											?>
											</div>
											<?php
										endif;
									?>
								</div>
								<div class="media-body">
									<div class="row m0 post_meta">
										<div class="row m0 date">
											<?php echo get_the_date(); ?>
										</div>
										<div class="row m0 author">
											<?php echo _e( 'Posted by', 'seowave' ); ?> 
											<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?> ">
											<?php the_author() ?></a>
										</div>
									</div>
									<a href="<?php echo esc_url( get_permalink() ); ?>" class="post_title">
										<h5><?php echo custom_excerpts(3); ?></h5>
									</a> 
								</div>
							</div>
						</div>
					</div>
					<?php
				endwhile;
						
				//Reset Original Data
				wp_reset_postdata();
			?>		
			</div>
			<?php
				echo seowave_content('<div class="row"><a href="','" class="view_all">'.esc_attr( $btntxt ).'</a></div>', esc_url( $btnurl ) );
			?>
		</div>
	</section>

	<?php
	return ob_get_clean();
}
add_shortcode('ow_blog', 'ow_blog');

/* - Blog */
vc_map( array(
	"name" => __("Blog", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_blog",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'seowave'),
			"param_name" => "title",
			"holder" => "div",
		),
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Description", 'seowave'),
			"param_name" => "desc",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Text", 'seowave'),
			"param_name" => "btntxt",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button URL", 'seowave'),
			"param_name" => "btnurl",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Post Per Page Display", 'seowave'),
			"param_name" => "posts_display",
			"holder" => "div",
		),
	)
) );