<?php
function ow_contact_form( $atts, $content = null ) {
	
	extract( shortcode_atts(
	
		array(
			'title' => '',
			'stitle_one' => '',
			'stitle_two' => '',
		), $atts )
	);

	ob_start();
	?>
	<section class="row">
		<div class="page_contact ">
			<div class="contact_form contentRow">
				<div class="container">
					<div class="row">
						<div class="row m0 title_row">
                           <?php echo seowave_content('<h2>','</h2>', esc_attr( $title ) ); ?>
                            <h5><?php echo esc_attr( $stitle_one ); ?> <span><?php echo esc_attr( $stitle_two ); ?></span></h5>
                        </div>
						<div class="formArea contact-form">
							<?php echo do_shortcode( $content ); ?>				
						</div>
					</div>
				</div>
			</div> <!--Contact Form-->
		</div>
	</section>
	
	<?php
	return ob_get_clean();
}
add_shortcode('ow_contact_form', 'ow_contact_form');

/* - Contact Form */
vc_map( array(
	"name" => __("Contact Form", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_contact_form",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'seowave'),
			"param_name" => "title",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Sub Title 1", 'seowave'),
			"param_name" => "stitle_one",
		),	
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Sub Title 2", 'seowave'),
			"param_name" => "stitle_two",
		),		
		array(
			"type" => "textarea_html",
			"class" => "",
			"heading" => __("Contact Form Shortcode", 'seowave'),
			"description" => __('Here Add Contact Form 7 Shortcode, e.g : [contact-form-7 id="245" title="Contact Page Form"]', 'seowave'),
			"param_name" => "content",
		),
	)
) );