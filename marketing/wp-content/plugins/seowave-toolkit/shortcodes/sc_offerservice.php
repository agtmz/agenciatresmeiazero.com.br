<?php
function sc_offerservice_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'titlefirst' => '',
		'titlesecond' => '',
		'desc' => '',
		'btntxtfirst' => '',
		'btnurlfirst' => '',
		'btntxtsecond' => '',
		'btnturlsecond' => '',
		'extra_class' => ''

	), $atts ) );

	$result = "
		<section class='row'>
        <div class='row m0 contentRow offer_service'>
            <div class='container'>
                <div class='row'>
                    <div class='offer_service_inner'>
                        <h2>$titlefirst<br class='hidden-xs'>$titlesecond</h2>
                        <p>$desc</p>
                        <ul class='nav'>
							 ".do_shortcode( $content )."
                        </ul>
						<div class='button-text'> 
							<a href='".esc_url($btnurlfirst)."' class='learn_more borderred_link fleft'><span>$btntxtfirst</span></a>
							<a href='".esc_url($btnurlfirst)."' class='borderred_link fleft quote_btn'><span>$btntxtsecond</span></a> 
						</div>
                    </div>
                </div>
            </div>
        </div>
	</section>";

	return $result;
}
add_shortcode( 'offerservice_outer', 'sc_offerservice_outer' );

function sc_offerservice_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'iconimg' => '',
		'title' => '',
		'extra_class' => ''

	), $atts ) );

	$icon_img = wp_get_attachment_image( $iconimg, 'seowave-23-31' );
	
	$result = "
		<li>
			$icon_img
			$title
		</li>";

	return $result;
}
add_shortcode( 'offerservice_inner', 'sc_offerservice_inner' );

// Parent Element
function vc_offerservice_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Offer Service", "seowave"),
		"base" => "offerservice_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'offerservice_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "textfield",
				"heading" => __("Title First Text", "seowave"),
				"param_name" => "titlefirst",
			),
			array(
				"type" => "textfield",
				"heading" => __("Title Last Text", "seowave"),
				"param_name" => "titlesecond",
			),
			array(
				"type" => "textarea",
				"heading" => __("Description", "seowave"),
				"param_name" => "desc",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button Text One", "seowave"),
				"param_name" => "btntxtfirst",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button URL One", "seowave"),
				"param_name" => "btnurlfirst",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button Text Seocnd", "seowave"),
				"param_name" => "btntxtsecond",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button URL Second", "seowave"),
				"param_name" => "btnurlsecond",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_offerservice_outer' );

// Nested Element
function vc_offerservice_inner() {

	vc_map( array(
		"name" => __("Single Offer Service", "seowave"),
		"base" => "offerservice_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'offerservice_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			
			array(
				"type" => "attach_image",
				"heading" => __("Icon Image", "seowave"),
				"param_name" => "iconimg",
			),
			
			array(
				"type" => "textfield",
				"heading" => __("Offer Title", "seowave"),
				"param_name" => "title",
			),
		)
	) );
}
add_action( 'vc_before_init', 'vc_offerservice_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Offerservice_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Offerservice_Inner extends WPBakeryShortCode {
    }
}
?>