<?php
function sc_ourprocess_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'title' => '',
		'desc' => '',
		'btntxt' => '',
		'btnurl' => '',
		'bg' => '',
		
		'extra_class' => ''

	), $atts ) );
	
	if( seowave_checkstring( $bg ) ) {
		
		$style= 'background-image:url('.wp_get_attachment_url($bg).');';
	}
	else {
		$style = "";
	}
	
	$result = "
		<div class='row m0 successive_process contentRow $extra_class'>
			<div class='container'>
				<div class='row title_row'>
					<h2>$title</h2>
					<h5>$desc</h5>
				</div>
			</div>
			<div class='row m0 successive_process_inner' style='".($style)."'>
				<div class='container'>
					<div class='row'>
						".do_shortcode( $content )."
					</div>
				</div>
			</div>
			<div class='container'>
				<div class='row text-center'>
					<a href='".esc_url( $btnurl )."' class='borderred_link quote_btn'>
						<span class='lh45'>$btntxt</span>
					</a> 
				</div>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'ourprocess_outer', 'sc_ourprocess_outer' );

function sc_ourprocess_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'process_image' => '',
		'title' => '',
		'subtitle' => '',
		'title' => '',
		'number' => '',
		
		'extra_class' => ''

	), $atts ) );

	$process_img = wp_get_attachment_image( $process_image, 'seowave_64_64' );

	$result = "
		<div class='fleft process $extra_class'>
			<div class='dot_circle'>
				<div class='icon_circle row m0'> 
					$process_img
					<div class='hover_texts'>
						$subtitle
					</div>
				</div>
			</div>
			<div class='row m0'>$number $title</div>
		</div>";

	return $result;
}
add_shortcode( 'ourprocess_inner', 'sc_ourprocess_inner' );

// Parent Element
function vc_ourprocess_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Ourprocess", "seowave"),
		"base" => "ourprocess_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'ourprocess_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
			),
			array(
				"type" => "textarea",
				"heading" => __("Description", "seowave"),
				"param_name" => "desc",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button Text", "seowave"),
				"param_name" => "btntxt",
			),
			array(
				"type" => "textfield",
				"heading" => __("Button Url", "seowave"),
				"param_name" => "btnurl",
			),
			array(
				"type" => "attach_image",
				"heading" => __("Background Image", "seowave"),
				"param_name" => "bg",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_ourprocess_outer' );

// Nested Element
function vc_ourprocess_inner() {

	vc_map( array(
		"name" => __("Single Ourprocess", "seowave"),
		"base" => "ourprocess_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'ourprocess_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "attach_image",
				"heading" => __("Process Image", "seowave"),
				"param_name" => "process_image",
			),
			array(
				"type" => "textfield",
				"heading" => __("Process Number", "seowave"),
				"param_name" => "number",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Sub Title", "seowave"),
				"param_name" => "subtitle",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		)
	) );
}
add_action( 'vc_before_init', 'vc_ourprocess_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Ourprocess_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Ourprocess_Inner extends WPBakeryShortCode {
    }
}
?>