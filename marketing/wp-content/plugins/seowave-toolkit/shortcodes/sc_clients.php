<?php
function sc_clients_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'title' => '',
		'desc' => '',

		'extra_class' => ''

	), $atts ) );

	$result = "
		<div class='row m0 our_clients $extra_class'>
			<div class='container'>
				<div class='row'>
					<div class='col-sm-3'>
						<div class='row m0'>
							<h2>$title</h2>
							<p>$desc</p>
						</div>
					</div>
					<div class='col-sm-9'>
						<div class='row'>
							<div class='owl-carousel clients_carousel'>
								".do_shortcode( $content )."
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'clients_outer', 'sc_clients_outer' );

function sc_clients_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'client_image' => '',
		'imglink' => '',
		
		'extra_class' => ''

	), $atts ) );

	$client_img = wp_get_attachment_image( $client_image, 'seowave_109_47' );
		
	$result = "
		<div class='item'>
			<a href='".esc_url( $imglink )."'>
				$client_img
			</a>
		</div>";

	return $result;
}
add_shortcode( 'clients_inner', 'sc_clients_inner' );

// Parent Element
function vc_clients_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("Clients", "seowave"),
		"base" => "clients_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'clients_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
				"holder" => "div",
			),
			array(
				"type" => "textarea",
				"heading" => __("Description", "seowave"),
				"param_name" => "desc",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_clients_outer' );

// Nested Element
function vc_clients_inner() {

	vc_map( array(
		"name" => __("Single Clients", "seowave"),
		"base" => "clients_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'clients_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "attach_image",
				"heading" => __("Clients Image", "seowave"),
				"param_name" => "client_image",
			),
			array(
				"type" => "textfield",
				"heading" => __("Image Link", "seowave"),
				"param_name" => "imglink",
				"holder" => "div",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		)
	) );
}
add_action( 'vc_before_init', 'vc_clients_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_Clients_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_Clients_Inner extends WPBakeryShortCode {
    }
}
?>