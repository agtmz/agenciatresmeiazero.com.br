<?php
function sc_about_outer( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'about_image' => '',
		'title' => '',
		'extra_class' => ''

	), $atts ) );

	$about_img = wp_get_attachment_image( $about_image, 'seowave_549_396' );

	$result = "
		<div class='row m0 contentRow about_company $extra_class'>
			<div class='container'>
				<div class='row'>
					<div class='col-sm-6'>
						$about_img
					</div>
					<div class='col-sm-6'>
						<h2>$title</h2>
						<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>
							".do_shortcode( $content )."
						</div>
					</div>
				</div>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'about_outer', 'sc_about_outer' );

function sc_about_inner( $atts, $content = null ) {

	extract( shortcode_atts( array(

		'accordion_image' => '',
		'title' => '',
		'desc' => '',
		
		'extra_class' => ''

	), $atts ) );

	$accordion_img = wp_get_attachment_image( $accordion_image, 'seowave-27-27' );
	
	$heading = to_slug( trim( $title ) );
	$result = "
		<div class='panel panel-default $extra_class'>
			<div class='panel-heading' role='tab' id='heading-$heading'>
				<h4 class='panel-title'>
					<a data-toggle='collapse' data-parent='#accordion' href='#collapse-$heading' aria-expanded='true' aria-controls='collapse-$heading'>
						<span class='icon'>
							$accordion_img
						</span>
							$title
						<span class='sign'></span>
					</a>
				</h4>
			</div>
			<div id='collapse-$heading' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading-$heading'>
				<div class='panel-body'>
					$desc
				</div>
			</div>
		</div>";

	return $result;
}
add_shortcode( 'about_inner', 'sc_about_inner' );

// Parent Element
function vc_about_outer() {

	// Register "container" content element. It will hold all your inner (child) content elements
	vc_map( array(
		"name" => __("About", "seowave"),
		"base" => "about_outer",
		"category" => esc_html__('Seowave', 'seowave'),
		"as_parent" => array('only' => 'about_inner'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"show_settings_on_create" => true,
		"is_container" => true,
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "attach_image",
				"heading" => __("About Image", "seowave"),
				"param_name" => "about_image",
			),
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
			),
			array(
				"type" => "textfield",
				"heading" => __("Extra class name", "seowave"),
				"param_name" => "el_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "seowave")
			)
		),
		"js_view" => 'VcColumnView'
	) );
}
add_action( 'vc_before_init', 'vc_about_outer' );

// Nested Element
function vc_about_inner() {

	vc_map( array(
		"name" => __("Single About", "seowave"),
		"base" => "about_inner",
		"category" => esc_html__('Seowave', 'seowave'),
		"content_element" => true,
		"as_child" => array('only' => 'about_outer'), // Use only|except attributes to limit parent (separate multiple values with comma)
		"params" => array(
			// add params same as with any other content element
			array(
				"type" => "attach_image",
				"heading" => __("About Icon Image", "seowave"),
				"param_name" => "accordion_image",
			),
			array(
				"type" => "textfield",
				"heading" => __("Title", "seowave"),
				"param_name" => "title",
				"holder" => "div",
			),
			array(
				"type" => "textarea",
				"heading" => __("Description", "seowave"),
				"param_name" => "desc",
				"holder" => "div",
			),
		)
	) );
}
add_action( 'vc_before_init', 'vc_about_inner' );

// Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {

    class WPBakeryShortCode_About_Outer extends WPBakeryShortCodesContainer {
	}
}

if ( class_exists( 'WPBakeryShortCode' ) ) {

    class WPBakeryShortCode_About_Inner extends WPBakeryShortCode {
    }
}
?>