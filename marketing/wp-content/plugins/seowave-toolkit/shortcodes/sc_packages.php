<?php
function ow_packages( $atts, $content = null ) {

	extract( shortcode_atts(
		array(
			'title' => '',
			'desc' => '',
			'posts_display' => 4,
		), $atts )
	);

	ob_start();

	/* Post Arguments */
	$args = array(
		'post_type' => 'product',
		'posts_per_page' => $posts_display,
	);

	$qry = new WP_Query( $args );
	?>
	<section class="row">
	
        <div class="container">
		
            <div class="row page_packages contentRow">
			
                <div class="row m0 title_row">
					<?php
						echo seowave_content('<h2>','</h2>',esc_attr( $title ) );
						echo seowave_content('<h5>','</h5>',esc_attr( $desc ) );
					?>
                </div>
				
                <div class="row m0 packages">
					
					<?php
					while( $qry->have_posts() ) : $qry->the_post();
						global $product;
						?>
						<div class="col-sm-6 package">
							<div class="row inner">
								<div class="col-sm-6"><?php the_post_thumbnail(); ?></div>
								<div class="col-sm-6">
									<h4 class="package_title"><?php the_title(); ?></h4>
									<?php if( class_exists( 'Woocommerce' ) ) { ?><h2 class="package_price price"><?php echo html_entity_decode( $product->get_price_html() ); ?></h2><?php } ?>
									<h6 class="package_period"><?php esc_html_e('Per ', 'seowave'); ?><?php echo get_post_meta( get_the_ID(), 'ow_cf_price_per', true ); ?></h6>
									<a href="<?php echo esc_url( the_permalink() ); ?>" class="details"><?php esc_html_e('more details', 'seowave'); ?></a>
								</div>
							</div>
						</div> <!--packages-->
						<?php
					endwhile;

					// Reset Original Data
					wp_reset_postdata();
					?>
					
                </div>
				
            </div>
			
        </div>
		
    </section>
	<?php
	return ob_get_clean();
}
add_shortcode('ow_packages', 'ow_packages');

/* - Our Applications */
vc_map( array(
	"name" => __("Packages", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_packages",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title", 'seowave'),
			"param_name" => "title",
			"holder" => "div",
		),
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Description", 'seowave'),
			"param_name" => "desc",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("No fo Packages Display", 'seowave'),
			"param_name" => "posts_display",
			"holder" => "div",
		),
	)
) );