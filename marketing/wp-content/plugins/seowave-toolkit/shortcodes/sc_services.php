<?php
function ow_services( $atts ) {

	extract( shortcode_atts(
		array(
		'title' => '',
		'titlefirst' => '',
		'titlelast' => '',
		'subtitlefirst' => '',
		'subtitlelast' => '',
		'desc' => '',
		'btntext' => '',
		'btnurl' => '',
		'per_page' => '',
		'owlayout' => ''
		), $atts )
	);

	if( '' === $per_page ) :
		$per_page = 6;
	endif;

	if( '' === $owlayout ) :
		$owlayout = "layout_one";
	endif;

	$args = array(
		'post_type' => 'ow_services',
		'posts_per_page' => $per_page,
		'order'   => 'ASC',
	);

	$qry = new WP_Query( $args );

	ob_start();
	
	if( $owlayout == "layout_one" ) {
	
		?>
		<section class="row <?php echo esc_attr( $owlayout ); ?>">
			<div class="container-fluid">
				<div class="row">
					<div class="media discover_services">
						<div class="media-left heading_secs col-sm-5 col-md-3">
							<h2 class="discover_heading"><?php echo esc_attr($titlefirst) ?><span><?php echo esc_attr($titlelast); ?></span></h2>
							
							<h5 class="sub_heading"><?php echo esc_attr( $subtitlefirst ); ?><span><?php echo esc_attr( $subtitlelast ); ?></span></h5>
							
							<p class="about"><?php echo esc_html( $desc ); ?></p>
							
							<?php echo seowave_content('<a href="'.esc_url($btnurl).'" class="subscribe_btn btn"><span>','</span></a>', esc_attr( $btntext ) ); ?>
						</div>
						<?php 
							if( $qry->have_posts() ) {
								?>
								<div class="media-body services_secs col-sm-7 col-md-9">
									<?php
										while ( $qry->have_posts() ) : $qry->the_post();
											?>
											<div class="col-sm-4 service_box">
												<div class="row service_box_inner">
													<div class="row preview_box"> 
														<?php 
															the_post_thumbnail( 'full' );
															the_title('<h6>','</h6>'); 
														?>
													</div>
													<div class="row hover_box">
														<div class="row m0 inner">
															<?php the_title('<h6>','</h6>'); ?>
															<div class="about"> 
															<?php echo wpautop( get_post_meta( get_the_ID(), 'ow_cf_content_desc', true ) ); ?>
															</div>
															<a href="<?php echo esc_url( get_permalink() ); ?>" class="read_more"><?php _e('Saiba mais','seowave'); ?></a> 
														</div>
													 </div>
												</div>
											</div>
											<?php
											
										endwhile;
										
									// Reset Post Data
									wp_reset_postdata();
									?>
								</div>
								<?php
							}
						?>
					</div>
				</div>
			</div>
		</section>
		<?php
	}
	elseif( $owlayout == "layout_two" ) {
		?>
		<section class="row <?php echo esc_attr( $owlayout ); ?>">
			<div class="container">
				<div class="row page_services contentRow">
					<div class="row m0 title_row">
						<?php 
							echo seowave_content('<h2>','</h2>', esc_attr( $title ) );
							echo seowave_content('<h5>','</h5>', esc_attr( $desc ) );
						?>
					</div>
					<?php 
						if( $qry->have_posts() ) {
							?>
							<div class="row m0">
								<?php
									while ( $qry->have_posts() ) : $qry->the_post();
										?>
										<div class="col-sm-4 service_box">
											<div class="row service_box_inner">
												<div class="row preview_box">
													<?php 
														the_post_thumbnail( 'full' );
														the_title('<h6>','</h6>'); 
													?>
												</div>
												<div class="row hover_box">
													<div class="row m0 inner">
														<?php the_title('<h6>','</h6>'); ?>
														<div class="about">
															<?php echo wpautop( get_post_meta( get_the_ID(), 'ow_cf_content_desc', true ) ); ?>
														</div>
														<a href="<?php echo esc_url( get_permalink() ); ?>" class="read_more"><?php _e('Read More','seowave'); ?></a>
													</div>
												</div>
											</div>
										</div>
										<?php
													
									endwhile;
												
								// Reset Post Data
								wp_reset_postdata();
								?>
							</div>
							<?php
						}
					?>
				</div>
			</div>
		</section>
		<?php
	}
	return ob_get_clean();
}
add_shortcode('ow_services', 'ow_services');

/* - Services */
vc_map( array(
	"name" => __("Services", 'seowave'),
	"icon" => 'vc-site-icon',
	"base" => "ow_services",
	"category" => __('Seowave', 'seowave'),
	"params" => array(
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title Single Line", 'seowave'),
			"param_name" => "title",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title FirstText", 'seowave'),
			"param_name" => "titlefirst",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Title LastText", 'seowave'),
			"param_name" => "titlelast",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("SubTitle FirstText", 'seowave'),
			"param_name" => "subtitlefirst",
			"holder" => "div",
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("SubTitle LastText", 'seowave'),
			"param_name" => "subtitlelast",
			"holder" => "div",
		),
		
		array(
			"type" => "textarea",
			"class" => "",
			"heading" => __("Short Description", 'seowave'),
			"param_name" => "desc",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button Text", 'seowave'),
			"param_name" => "btntext",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Button URL", 'seowave'),
			"param_name" => "btnurl",
			"holder" => "div",
		),
		
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Post Per Page", 'seowave'),
			"param_name" => "per_page",
			"holder" => "div",
		),
		
		array(
			'type' => 'dropdown',
			'class' => '',
			'heading' => __( 'Layout', 'seowave' ),
			'param_name' => 'owlayout',
			'value' =>array(
				'Layout 1' => 'layout_one',
				'Layout 2' => 'layout_two',
			),
			'description' => __( 'Select Post Layout. Default Layout 1 Select', 'seowave' ),
		),
	)
) );