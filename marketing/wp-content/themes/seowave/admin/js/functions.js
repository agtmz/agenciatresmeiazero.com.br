(function($) {

	/* Event - Document Ready */	
	$(document).ready(function($) {

		/* Disable : Page Editor */
		if( $('body.post-type-page #postdivrich').length ) {

			/* Sidebar Layout */
			if( $('select#page_template').val() != 'default' ) {
				$('body.post-type-page #postdivrich').slideUp(500);
			}

			$('select#page_template').live('change', function() {

				/* Sidebar Layout */
				if( $('select#page_template').val() != 'default' ) {
					$('body.post-type-page #postdivrich').slideUp(500);
				}
				else {
					$('body.post-type-page #postdivrich').slideDown(500);
					$(window).scrollTop($(window).scrollTop()+1);
				}

			});
		}

		/* Post : Metabox */
		if( $('#ow_cf_metabox_post_layout').length ) {

			/* Sidebar Layout */
			if( $('select#ow_cf_sidebar_layout_post').val() == 'no_sidebar' ) {
				$('.cmb2-id-ow-cf-widget-area-post').slideUp(500);
			}

			$('select#ow_cf_sidebar_layout_post').live('change', function() {

				/* Sidebar Layout */
				if( $('select#ow_cf_sidebar_layout_post').val() == 'no_sidebar' ) {
					$('.cmb2-id-ow-cf-widget-area-post').slideUp(500);
				}
				else {
					$('.cmb2-id-ow-cf-widget-area-post').slideDown(500);
				}

			});
		}

		/* Post : Formats */
		if( $('#post-formats-select').length ) {

			if( $('input[id="post-format-gallery"]').is(':checked') ) {
				$('#ow_cf_metabox_post_gallery').slideDown(500); /* Enable Gallery */
			}
			else {
				$('#ow_cf_metabox_post_gallery').slideUp(500);
			}

			/* On Change : Event */
			$('#post-formats-select').live('change', function() {
				if( $('input[id="post-format-gallery"]').is(':checked') ) {
					$('#ow_cf_metabox_post_gallery').slideDown(500); /* Enable Gallery */
				}
				else {
					$('#ow_cf_metabox_post_gallery').slideUp(500);
				}
			});
		}
		
		if( $('#post-formats-select').length ) {

			if( $('input[id="post-format-quote"]').is(':checked') ) {
				$('#ow_cf_metabox_post_quote').slideDown(500); /* Enable Quote */
			}
			else {
				$('#ow_cf_metabox_post_quote').slideUp(500);
			}

			/* On Change : Event */
			$('#post-formats-select').live('change', function() {
				if( $('input[id="post-format-quote"]').is(':checked') ) {
					$('#ow_cf_metabox_post_quote').slideDown(500); /* Enable Quote */
				}
				else {
					$('#ow_cf_metabox_post_quote').slideUp(500);
				}
			});
		}


		/* Page : Metabox */
		if( $('#ow_cf_metabox_page').length ) {

			/* Header Background Color */
			if( $('select#ow_cf_page_title').val() != 'enable' ) {
				$('.cmb2-id-ow-cf-page-header-img').slideUp(500);
				$('.cmb2-id-ow-cf-page-sub-title').slideUp(500);
			}

			$('#ow_cf_page_title').live('change', function() {

				/* Header Background Image */
				if( $('select#ow_cf_page_title').val() == 'disable' ) {
					$('.cmb2-id-ow-cf-page-header-img').slideUp(500);
					$('.cmb2-id-ow-cf-page-sub-title').slideUp(500);
				}
				else {
					$('.cmb2-id-ow-cf-page-header-img').slideDown(500);
					$('.cmb2-id-ow-cf-page-page-sub-title').slideDown(500);
				}
			});

			/* Sidebar Layout - Page */
			if( $('select#ow_cf_sidebar_layout').val() == 'no_sidebar' ) {
				$('.cmb2-id-ow-cf-widget-area').slideUp(500);
			}

			$('select#ow_cf_sidebar_layout').live('change', function() {

				/* Sidebar Layout - Page */
				if( $('select#ow_cf_sidebar_layout').val() == 'no_sidebar' ) {
					$('.cmb2-id-ow-cf-widget-area').slideUp(500);
				}
				else {
					$('.cmb2-id-ow-cf-widget-area').slideDown(500);
				}

			});
		}

		// Uploads			
		var seowave_img_frame;

		$(document).on('click', 'input.select-img', function( event ) {

			var $this = $(this);

			event.preventDefault();

			var SEOWAVEImage = wp.media.controller.Library.extend({
				defaults :  _.defaults({
						id:        'seowave-insow-image',
						title:      $this.data( 'uploader_title' ),
						allowLocalEdits: false,
						displaySettings: true,
						displayUserSettings: false,
						multiple : false,
						library: wp.media.query( { type: 'image' } )
				  }, wp.media.controller.Library.prototype.defaults )
			});

			// Create the media frame.
			seowave_img_frame = wp.media.frames.seowave_img_frame = wp.media({
			  button: {
				text: $( this ).data( 'uploader_button_text' ),
			  },
			  state : 'seowave-insow-image',
				  states : [
					  new SEOWAVEImage()
				  ],
			  multiple: false  // Set to true to allow multiple files to be selected
			});

			// When an image is selected, run a callback.
			seowave_img_frame.on( 'select', function() {

				var state = seowave_img_frame.state('seowave-insow-image');
				var selection = state.get('selection');
				var display = state.display( selection.first() ).toJSON();
				var obj_attachment = selection.first().toJSON();

				display = wp.media.string.props( display, obj_attachment );

				var image_field = $this.siblings('.img');
				var imgurl = display.src;

				// Copy image URL
				image_field.val(imgurl);

				// Show in preview
				var image_preview_wrap = $this.siblings('.seowave-preview-wrap');
				image_preview_wrap.show();
				image_preview_wrap.find('img').attr('src',imgurl);
			});

			// Finally, open the modal
			seowave_img_frame.open();
		});
	});
})(jQuery);