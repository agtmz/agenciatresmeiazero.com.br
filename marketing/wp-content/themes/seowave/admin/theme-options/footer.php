<?php
/* Footer Settings */
Redux::setSection( $opt_name, array(
	'title'  => __( 'Footer Settings', 'seowave' ),
	'icon' => 'el el-wrench-alt',
	'subsection' => false,
	'fields'     => array(
		/* Fields */
		
		array(
			'id'=>'opt_footer_phone',
			'type' => 'text',
			'title' => __('Contact No', 'seowave'),
			'default'  => __('+(61) 123 456 7890', 'seowave')
		),
		
		array(
			'id'=>'opt_footer_email',
			'type' => 'text',
			'title' => __('Emial', 'seowave'),
			'default'  => __('info@example.com', 'seowave')
		),
		
		array(
			'id'=>'opt_footer_fax',
			'type' => 'text',
			'title' => __('Fax', 'seowave'),
			'default'  => __('+(61) 123 456 7890', 'seowave')
		),

		array(
			'id'=>'info_copyright',
			'type' => 'info',
			'title' => __('Copyright Section', 'seowave' ),
		),
		array(
			'id'       => 'opt_footer_copyright',
			'type'     => 'editor',
			'title'    => __( 'Copyright Text', 'seowave' ),
			'subtitle' => __( 'Use any of the features of WordPress editor inside your panel!', 'seowave' ),
			'default'  => 'Copyright &copy; 2017 <a href="#">Seowave</a>. All Rights Reserved.',
		),
		/* Fields /- */	
	),
));
?>