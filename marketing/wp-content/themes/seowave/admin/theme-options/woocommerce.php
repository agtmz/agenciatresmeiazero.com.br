<?php
/* Woocommerce */
Redux::setSection( $opt_name, array(
	'title'  => __( 'Woocommerce', 'seowave' ),
	'icon' => 'el el-shopping-cart',
	'subsection' => false,
	'fields'     => array(
		/* Fields */
		array(
			'id'=>'info_wc_pd_widget_setting',
			'type' => 'info',
			'title' => __('Product Single', 'seowave' ),
		),
		array(
			'id'       => 'opt_wc_sidebar',
			'type'     => 'switch',
			'title'    => __( 'Sidebar', 'seowave' ),
			'default'  => 1,
			'on'       => 'Enabled',
			'off'      => 'Disabled',
		),
		array(
			'id'       => 'opt_wc_widget_area',
			'type'     => 'select',
			'title'    => 'Select Widget Area',
			'data'     => 'sidebars',
			'default'  => array( 'sidebar-3' ),
			'required' => array( 'opt_wc_sidebar', '=', '1' ),
		),
		/* Fields /- */	
	)
));