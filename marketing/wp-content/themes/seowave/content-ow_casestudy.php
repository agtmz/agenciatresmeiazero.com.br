<?php
/**
 *
 * @package WordPress
 * @subpackage Seowave
 * @since Seowave 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<section class="row page_case_study">
        <nav class="paginations case_study_detail_pagination">
            <div class="container">
                <div class="row">
                   <?php
					the_post_navigation( array(
						'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next ', 'seowave' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Next post:', 'seowave' ) . '</span> ' .
							'<span class="post-title">%title</span>',
						'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( ' Previous', 'seowave' ) . '</span> ' .
							'<span class="screen-reader-text">' . __( 'Previous post:', 'seowave' ) . '</span> ' .
							'<span class="post-title">%title</span>',
					) );
				   ?>
                </div>
            </div>
        </nav>
        <div class="row m0">
            <div class="container">
                <div class="row image_1">
					<?php
					if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_portfolio_bigimage_id', true ) ) ) :
						echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_portfolio_bigimage_id', true), 'full' );
					endif; ?>
				</div>
            </div>
        </div>
        <div class="trenz_master">
            <div class="container">
				<div class="col-sm-8">
					<?php 
						the_title('<h2 class="trenz_master_title page_title">','</h2>');
						echo wpautop( get_post_meta( get_the_ID(), 'ow_cf_casestudy_desc', true ) );
					?>
				</div>
				<div class="col-sm-3 col-sm-offset-1">
					<?php
						if(seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_client_name', true ) ) ) {
							?>
							<div class="casestudy-info">
								<span><?php _e('Client:','seowave'); ?></span>
								<?php echo esc_attr( get_post_meta( get_the_ID(), 'ow_cf_client_name', true ) ) ?>
							</div>
							<?php
						}
					
						if(seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_project_type', true ) ) ) {
							?>
							<div class="casestudy-info">
								<span><?php _e('Project type:','seowave'); ?></span> 
								<?php echo esc_attr( get_post_meta( get_the_ID(), 'ow_cf_project_type', true ) ) ?>
							</div>
							<?php
						}
					
						if(seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_web_url', true ) ) ) {
							?>
							<div class="casestudy-info">
								<span><?php _e('Website:','seowave'); ?></span> 
								<a href="<?php echo esc_url( get_post_meta( get_the_ID(), 'ow_cf_web_url', true ) ) ?>"><?php echo esc_attr( get_post_meta( get_the_ID(), 'ow_cf_web_url', true ) ) ?></a>
							</div>
							<?php
						}
					?>
				</div>
            </div>
        </div>
        <div class="traffic">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
						<?php
							if(seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_counter_value', true ) ) ) {
								?>
								<h2>
									<span class="counter"><?php echo esc_attr( get_post_meta( get_the_ID(), 'ow_cf_counter_value', true ) ) ?>
									</span><?php _e('%','seowave'); ?>
								</h2>
								<?php
							}

							echo seowave_content('<h4>','</h4>', esc_attr( get_post_meta( get_the_ID(), 'ow_cf_counter_title', true ) ) );
							
							if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_counter_img_id', true ) ) ) :
								echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_counter_img_id', true), 'seowave-550-164' );
							endif;
						?>
                    </div>
                    <div class="col-sm-6">
						<?php
							if(seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_counter_valueright', true ) ) ) {
								?>
								<h2>
									<span class="counter">
										<?php echo esc_attr( get_post_meta( get_the_ID(), 'ow_cf_counter_valueright', true ) ) ?></span>
										<?php _e('%','seowave'); ?>
								</h2>
								<?php
							}
							echo seowave_content('<h4>','</h4>', esc_attr( get_post_meta( get_the_ID(), 'ow_cf_counter_titleright', true ) ) );
                      
							if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_counter_imgright_id', true ) ) ) :
								echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_counter_imgright_id', true), 'seowave-550-166' );
							endif;
						?>
                    </div>
                </div>
            </div>
        </div>
        <div class="challenge">
            <div class="container">
				<div class="col-sm-5">
					<?php
						if(seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_challenge_desc', true ) ) ) {
							?>
							<h3><?php _e('Challenge','seowave'); ?></h3>
							<?php
						}
						echo wpautop( get_post_meta( get_the_ID(), 'ow_cf_challenge_desc', true ) );
						
						if(seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_solution_desc', true ) ) ) {
							?>
							<h3><?php _e('Solution','seowave'); ?></h3>
							<?php
						}
						echo wpautop( get_post_meta( get_the_ID(), 'ow_cf_solution_desc', true ) ); 
					?>
				</div>
				<div class="col-sm-6 col-sm-offset-1">
					<div class="row">
					<?php
						if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_single_img_id', true ) ) ) :
							echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_single_img_id', true), 'seowave-579-460' );
						endif;
					?>
					</div>
				</div>
            </div>
        </div>
        <div class="m0 results">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <div class="row">
							<?php
								if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_result_img_id', true ) ) ) :
									echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_result_img_id', true), 'seowave-497-265' );
								endif;
							?>
                        </div>
                    </div>
                    <div class="col-sm-6">
					<?php 
						if(seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_result_desc', true ) ) ) {
							?>
							<h3><?php _e('Results','seowave'); ?></h3>
							<?php
						}
						echo wpautop( get_post_meta( get_the_ID(), 'ow_cf_result_desc', true ) ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="m0 share_row">
            <div class="container">
                <div class="row">
                    <ul class="list-inline share_buttons social-share">
                        <li class="facebook"><a href="javascript: void(0)" data-action="facebook" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-facebook-square"></i><?php esc_html_e('Share', 'seowave'); ?></a></li>
						<li class="twitter"><a href="javascript: void(0)" data-action="twitter" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-twitter"></i><?php esc_html_e('Twitter', 'seowave'); ?></a></li>
						<li class="google-plus"><a href="javascript: void(0)" data-action="google-plus" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-google-plus"></i><?php esc_html_e('Google Plus', 'seowave'); ?></a></li>
						<li class="linkedin"><a href="javascript: void(0)" data-action="linkedin" data-title="<?php the_title(); ?>" data-url="<?php echo esc_url(the_permalink()); ?>"><i class="fa fa-linkedin"></i><?php esc_html_e('Linkedin', 'seowave'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

</article>