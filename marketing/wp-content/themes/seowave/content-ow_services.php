<?php
/**
 * The template for displaying a "No posts found" message
 *
 * @package WordPress
 * @subpackage Seowave
 * @since Seowave 1.0
 */
 
$args = array(
	'post_type' => 'ow_services'
);
$qry = new WP_Query($args);
$current_post_id = $post->ID; 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	 <section class="row">
        <div class="container">
            <div class="row page_service_details">
                <div class="service_descs">
                    <div class="m0">
						<div class="m0">
							<div class="fleft fpara">
								<?php the_title('<h3 class="sv_titile">','</h3>'); ?>
								<div class="sv_about">
                                   <div class="f12"><?php echo wpautop( get_post_meta( get_the_ID(), 'ow_cf_service_desc', true ) ); ?></div>
                                   <ul class="list-unstyled">
									<?php
										$plan_features = get_post_meta( get_the_ID(), 'ow_cf_plan_features', true );
										
										if( isset( $plan_features ) && count( $plan_features ) > 0 && is_array( $plan_features ) ) {
											foreach( $plan_features as $plan_feature ) {
												?>
												<li><i class="fa fa-check"></i><?php echo esc_html( $plan_feature ); ?></li>
												<?php
											}
										}
									?>
                                   </ul>
								</div>
							</div>
							
						   <div class="fright big_img">
							   
								<?php if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_plan_mainimage_id', true ) ) ) :
										echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_plan_mainimage_id', true), 'seowave-360-368' );
									endif; ?>
							   </div>
						</div>
						<div class="m0">
						   <?php echo seowave_content('<h4>','</h4>', esc_attr( get_post_meta( get_the_ID(), 'ow_cf_content_title', true ) ) ); ?>
                           <div class="f13"><?php echo wpautop( get_post_meta( get_the_ID(), 'ow_cf_content_desc', true ) ); ?></div>
                           <ul class="nav sv_subjects">
								<?php 
									$seocontent_grp_txt = get_post_meta( get_the_ID(), 'ow_cf_services_grp', true );
									if( count( $seocontent_grp_txt ) > 0 && is_array( $seocontent_grp_txt ) ) {
										foreach ( (array) $seocontent_grp_txt as $key => $value ) {
											if ( isset( $value['content_img'] )|| isset( $value['keyword_title'] ) ) {
												?>
												<li>
													<div class="media">
														<div class="media-left">
															<span>
																<img src="<?php echo esc_url( $value['content_img'], 'seowave-26-26' ); ?>" alt="">
															</span>
														</div>
														<div class="media-body media-middle"><?php echo esc_html( $value['keyword_title'] ); ?></div>
													</div>
												</li>
												<?php
											}
										}
									}
								?>
                             
                           </ul>
						</div>
						<div class="m0 graph_row">
							<?php 
								echo seowave_content('<h5 class="sv_subtitle_center">','</h5>', esc_attr( get_post_meta( get_the_ID(), 'ow_cf_ranking_title', true ) ) ); 
								
								if( seowave_checkstring( get_post_meta( get_the_ID(), 'ow_cf_ranking_img_id', true ) ) ) :
									echo wp_get_attachment_image( get_post_meta( get_the_ID(), 'ow_cf_ranking_img_id', true), 'seowave-651-264' );
								endif;
							?>
						</div>
						<div class="m0">
							<h4 class="sv_subtitle"><?php _e('Some of Our Results','seowave'); ?></h4>
							<div class="m0 funFacts">
								<?php 
									$result_grp_txt = get_post_meta( get_the_ID(), 'ow_cf_services_grpresult', true );
									if( count( $result_grp_txt ) > 0 && is_array( $result_grp_txt ) ) {
										foreach ( (array) $result_grp_txt as $key => $value ) {
											if ( isset( $value['result_img'] )|| isset( $value['result_value'] ) || isset( $value['result_title'] ) ) {
												?>
												<div class="col-sm-4 fact">
													<h3>
														<img src="<?php echo esc_url( $value['result_img'], 'seowave-29-30' ); ?>" alt=""> 
														<span class="counter"><?php echo esc_html( $value['result_value'] ); ?></span><?php _e('%','seowave'); ?>
													</h3>
													<h6><?php echo esc_html( $value['result_title'] ); ?></h6>
												</div>
												<?php
											}
										}
									}
			
								?>
						
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</article>